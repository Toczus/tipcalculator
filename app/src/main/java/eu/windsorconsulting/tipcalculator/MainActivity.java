package eu.windsorconsulting.tipcalculator;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;

import java.text.NumberFormat;

public class MainActivity extends AppCompatActivity {

    // GUI - first row
    private EditText amountEditText;
    private TextView amountTextView;

    // GUI - second row
    private TextView percentTextView;
    private SeekBar percentSeekBar;

    // GUI - third row
    private TextView tipTextView;

    // GUI - fourth row
    private TextView totalTextView;

    // Static objects for formatting currency and percentage values
    private static final NumberFormat CURRENCY_FORMAT = NumberFormat.getCurrencyInstance();
    private static final NumberFormat PERCENT_FORMAT = NumberFormat.getPercentInstance();

    // Numeric fields that store the bill amount and tip percentage
    private double billAmount = 0.0;
    private double tipPercent = 0.15;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // GUI view fields initialization
        amountEditText = findViewById(R.id.amountEditText);
        amountTextView = findViewById(R.id.amountTextView);

        percentTextView = findViewById(R.id.percentTextView);
        percentSeekBar = findViewById(R.id.percentSeekBar);

        tipTextView = findViewById(R.id.tipTextView);

        totalTextView = findViewById(R.id.totalTextView);

        // Listening events for EditText field.
        amountEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                try {
                    billAmount = Double.parseDouble(s.toString()) / 100.00;
                    amountTextView.setText(CURRENCY_FORMAT.format(billAmount));
                } catch(NumberFormatException ex) {
                    amountTextView.setText("");
                    billAmount = 0.0;
                }
                calculateTipAndTotalAmount();
            }

            @Override
            public void afterTextChanged(Editable s) {}
        });

        // Listening for events for the SeekBar field
        percentSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                tipPercent = progress / 100.0;
                calculateTipAndTotalAmount();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {}

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {}
        });
    }

    private void calculateTipAndTotalAmount() {

        double tipAmount = billAmount * tipPercent;
        double totalAmount = billAmount + tipAmount;

        percentTextView.setText(PERCENT_FORMAT.format(tipPercent));
        tipTextView.setText(CURRENCY_FORMAT.format(tipAmount));
        totalTextView.setText(CURRENCY_FORMAT.format(totalAmount));
    }
}